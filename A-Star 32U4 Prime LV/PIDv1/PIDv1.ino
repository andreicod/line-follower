//de lucrat la oprire cand iese de pe traseu

#include "IRremote.h"

//calibrarea se face cu ajutorul telecomenzii
//varianta in care diferenta dintre sensori se face in functie de minimul dintre maxime, scazandu-se dif

int dir;
int nrsensori = 8, rightmotorspeed, leftmotorspeed;
unsigned long error, lasterror, suma;
float difmotoare;
 
#define X 10 //P% (la suta)
#define Y 5 //I% (la suta)
#define Z 120 //D% (la suta)

#define BASE_SPEED 130

#define NUM_SENSORI 8

#define SENSOR0 A11
#define SENSOR1 A6
#define SENSOR2 A0
#define SENSOR3 A1
#define SENSOR4 A2
#define SENSOR5 A3
#define SENSOR6 A4
#define SENSOR7 A5

#define LED 13

#define RECV_PIN 0

//pini control motoare
#define motorpwm1 10
#define motordigital1 8
#define motorpwm2 9
#define motordigital2 7

long int sensorerror[NUM_SENSORI];
int maxim[NUM_SENSORI];
int minim[NUM_SENSORI];
long int sensor[NUM_SENSORI];
int dif[NUM_SENSORI];
int minmax = 1000;

bool af = 0;

IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {  
  irrecv.enableIRIn();
  
  pinMode(LED, OUTPUT);
  
  pinMode(SENSOR0, INPUT);
  pinMode(SENSOR1, INPUT);
  pinMode(SENSOR2, INPUT);
  pinMode(SENSOR3, INPUT);
  pinMode(SENSOR4, INPUT);
  pinMode(SENSOR5, INPUT);
  pinMode(SENSOR6, INPUT);
  pinMode(SENSOR7, INPUT);
  
}


int x, y;
void motoare (int x, int y){
  if (y < -255)
    y = -255;
  if (x < -255)
    x = -255;
  if (y > 255)
    y = 255;
  if (x > 255)
    x = 255;
  if (x >= 0){
    analogWrite (motorpwm1, x);
    digitalWrite (motordigital1, 1);
    }

  if (x < 0){
    analogWrite (motorpwm1, -x);
    digitalWrite (motordigital1, 0);
  }

  if (y >= 0){
    analogWrite (motorpwm2, y);
    digitalWrite (motordigital2, 1);
  }

  if (y < 0){
    analogWrite (motorpwm2, -y);
    digitalWrite (motordigital2, 0);
  }
}

void loop()
{
  if (irrecv.decode(&results)) 
  {
    if (results.value == 1886388479)
    {
      //CALIBRAREA SENSORILOR INAINTE DE CURSA
      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
      //aprindem ledul
      digitalWrite (LED, HIGH);
      
      unsigned long int time = millis();
      unsigned long int starttime = millis();
    
      //minimul incepe de la o valoare cat mai mare (cea mai mare posibila pe care o poate sensorul) ca sa avem din ce scadea
      for (int i = 0; i < NUM_SENSORI; ++ i)
        minim[i] = 1000;

      for (int i = 0; i < NUM_SENSORI; ++ i)
        maxim[i] = 0;
            
      //stabilesc minimurile si maximurile pentru fiecare sensor in parte
      while (time - starttime <= 5000)
      {
        sensor[0] = analogRead(SENSOR0);
        sensor[1] = analogRead(SENSOR1);
        sensor[2] = analogRead(SENSOR2);
        sensor[3] = analogRead(SENSOR3);
        sensor[4] = analogRead(SENSOR4);
        sensor[5] = analogRead(SENSOR5);
        sensor[6] = analogRead(SENSOR6);
        sensor[7] = analogRead(SENSOR7);
    
    
        for (int i = 0; i < NUM_SENSORI; ++ i)
        {
          if (sensor[i] > maxim[i])
            maxim[i] = sensor[i];
          if (sensor[i] < minim[i])
            minim[i] = sensor[i];
        }
        
        time = millis();
      }
    
      //imi iau masuri de siguranta ca, cand sensorul e pe alb valoare lui sa fie 0
      for (int i = 0; i < NUM_SENSORI; ++ i)
        minim[i] = minim[i] + 30;
    
    
      //imi aleg cel mai mic dintre valorile maxime ale sensorilor
      for (int i = 0; i < NUM_SENSORI; ++ i)
        if (maxim[i] < minmax)
          minmax = maxim[i];
    
      //calculam sensorii in functie de minmax; diferenta dintre ei reprezinta dif[i], astfel mai incolo in program  cand din valoare sensorului scadem dif[i] valorile sunt cat mai apropriate unele de celelalte
      //a nu se uita ca imi iau si aici o marja de eroare
      for (int i = 0; i < NUM_SENSORI; ++ i)
        dif[i] = maxim[i] - minmax + 30;
        
      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }
    
    if (results.value == 1886398679)
      af = !af;
    
    if (af == 1)
    {
      delay (1000);
      error = 0;
    }
      
      while  (af == 1)
      {

      //AICI SE INTAMPLA MAGIA
      //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      


      while (error >= 100 && af == 1)
      {
        
        sensor[0] = analogRead(SENSOR0);
        sensor[1] = analogRead(SENSOR1);
        sensor[2] = analogRead(SENSOR2);
        sensor[3] = analogRead(SENSOR3);
        sensor[4] = analogRead(SENSOR4);
        sensor[5] = analogRead(SENSOR5);
        sensor[6] = analogRead(SENSOR6);
        sensor[7] = analogRead(SENSOR7);
    
        //facem calculul final pentru valoarea sensorilor
        for (int i = 0; i < NUM_SENSORI; ++ i)
        {
          sensor[i] = sensor[i] - minim[i] - dif[i];

          if (sensor[i] < 0)
             sensor[i] = 0;
          
          sensor[i] = sensor[i] / 10;
        }
        
        error = 0;
        suma = 0;
          
        for (int i = 0; i < NUM_SENSORI; ++ i)
            sensorerror[i] = sensor[i] * ((i + 1) * 100);
        
        for (int i = 0; i < NUM_SENSORI; ++ i)
            suma = suma + sensor[i];
        
        for (int i = 0; i < NUM_SENSORI; ++ i)
          error = error + sensorerror[i];
        
        if (suma > 0)
        {
          error = error / suma;
          difmotoare = ((X *((450.0 - error) / ((NUM_SENSORI - 1) * 50) * BASE_SPEED)) + (Y * (((450.0 - error) + (450.0 - lasterror)) / ((NUM_SENSORI - 1) * 100) * BASE_SPEED)) + (Z * (((450.0 - error) - (450.0 - lasterror)) / ((NUM_SENSORI - 1) * 100) * BASE_SPEED))) / 100;

        /*if ((int)difmotoare <= BASE_SPEED / 4 && (int)difmotoare >= - BASE_SPEED / 4)
        {
          /*if (difmotoare >= 0){
            rightmotorspeed = BASE_SPEED + ((int)difmotoare / 2);
            leftmotorspeed = BASE_SPEED;
          }
          else{
            rightmotorspeed = BASE_SPEED;
            leftmotorspeed = BASE_SPEED - ((int)difmotoare / 2);
          }*/
         /* rightmotorspeed = BASE_SPEED + ((int)difmotoare / 2);
          leftmotorspeed = BASE_SPEED - ((int)difmotoare / 2);
        }
        else
        {
          /*
          if (difmotoare >= 0){
            rightmotorspeed = BASE_SPEED;
            leftmotorspeed = ((BASE_SPEED / 4) * 3) - (((int)difmotoare / 4) * 3);
          }
          else{
            rightmotorspeed = ((BASE_SPEED / 4) * 3) + (((int)difmotoare / 4) * 3);
            leftmotorspeed =  BASE_SPEED;
          }
          */
         /* rightmotorspeed = BASE_SPEED + (((int)difmotoare / 4) * 3);
          leftmotorspeed = BASE_SPEED - (((int)difmotoare / 4) * 3);
        }*/
        rightmotorspeed = BASE_SPEED + (2 * (int)difmotoare);
        leftmotorspeed = BASE_SPEED - (2 * (int)difmotoare);
        
        if (rightmotorspeed > 255)
          rightmotorspeed = 255;
        if (leftmotorspeed > 255)
          leftmotorspeed = 255;
        if (rightmotorspeed < -255)
          rightmotorspeed = -255;
        if (leftmotorspeed < -255)
          leftmotorspeed = -255;
  
        motoare (leftmotorspeed, rightmotorspeed);

        lasterror = error;
        }
        
        if (digitalRead (RECV_PIN) == 0)
              af = !af;
      }
      
      if (lasterror > 450 && af == 1)
        while (error == 0 && af == 1)
        {
          motoare(BASE_SPEED, -BASE_SPEED / 6 * 4);

          dir = 3;
            
          sensor[0] = analogRead(SENSOR0);
          sensor[1] = analogRead(SENSOR1);
          sensor[2] = analogRead(SENSOR2);
          sensor[3] = analogRead(SENSOR3);
          sensor[4] = analogRead(SENSOR4);
          sensor[5] = analogRead(SENSOR5);
          sensor[6] = analogRead(SENSOR6);
          sensor[7] = analogRead(SENSOR7);
  
          //facem calculul final pentru valoarea sensorilor
          for (int i = 0; i < NUM_SENSORI; ++ i)
          {
            sensor[i] = sensor[i] - minim[i] - dif[i];

            sensor[i] = sensor[i] / 10;
        
            if (sensor[i] < 0)
              sensor[i] = 0;
          }
            
          for (int i = 0; i < NUM_SENSORI; ++ i)
            sensorerror[i] = sensor[i] * ((i + 1) * 100);
          for (int i = 0; i < NUM_SENSORI; ++ i)
            error = error + sensorerror[i];
            
          if (digitalRead (RECV_PIN) == 0)
            af = !af;
        }
        
        if (lasterror <= 450 && af == 1)
          while (error == 0 && af == 1)             
          {
            motoare(-BASE_SPEED / 6 * 4, BASE_SPEED);

            dir = 1;
            
            sensor[0] = analogRead(SENSOR0);
            sensor[1] = analogRead(SENSOR1);
            sensor[2] = analogRead(SENSOR2);
            sensor[3] = analogRead(SENSOR3);
            sensor[4] = analogRead(SENSOR4);
            sensor[5] = analogRead(SENSOR5);
            sensor[6] = analogRead(SENSOR6);
            sensor[7] = analogRead(SENSOR7);
  
            //facem calculul final pentru valoarea sensorilor
            for (int i = 0; i < NUM_SENSORI; ++ i)
            {
              sensor[i] = sensor[i] - minim[i] - dif[i];

              sensor[i] = sensor[i] / 10;
        
              if (sensor[i] < 0)
                sensor[i] = 0;
            }
            
            for (int i = 0; i < NUM_SENSORI; ++ i)
              sensorerror[i] = sensor[i] * ((i + 1) * 100);
            for (int i = 0; i < NUM_SENSORI; ++ i)
              error = error + sensorerror[i];
              
            if (digitalRead (RECV_PIN) == 0)
              af = !af;
           }
      
      suma = 0;
      for (int i = 0; i < NUM_SENSORI; ++ i)
        suma = suma + sensor[i];
      if (suma > 0)
      {
        error = error / suma;
        lasterror = error;
      }

      if (dir == 1)
        motoare(BASE_SPEED * 8 / 6, -BASE_SPEED / 2);
      if (dir == 3)
        motoare(-BASE_SPEED / 2, BASE_SPEED * 8 / 6);

      delay(20);
      
      //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
      if (digitalRead (RECV_PIN) == 0)
        af = !af;
    }
    
    
    
    if (af == 0)
      motoare (0, 0);
    
    digitalWrite (LED, LOW);
    delay (200);
    irrecv.resume();
  }
  
}

