#define NUM_SENSORI 8

#define SENSOR0 A11
#define SENSOR1 A6
#define SENSOR2 A0
#define SENSOR3 A1
#define SENSOR4 A2
#define SENSOR5 A3
#define SENSOR6 A4
#define SENSOR7 A5

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  pinMode(SENSOR0, INPUT);
  pinMode(SENSOR1, INPUT);
  pinMode(SENSOR2, INPUT);
  pinMode(SENSOR3, INPUT);
  pinMode(SENSOR4, INPUT);
  pinMode(SENSOR5, INPUT);
  pinMode(SENSOR6, INPUT);
  pinMode(SENSOR7, INPUT);
}

void loop() {
  int sensor0 = analogRead(SENSOR0);
  int sensor1 = analogRead(SENSOR1);
  int sensor2 = analogRead(SENSOR2);
  int sensor3 = analogRead(SENSOR3);
  int sensor4 = analogRead(SENSOR4);
  int sensor5 = analogRead(SENSOR5);
  int sensor6 = analogRead(SENSOR6);
  int sensor7 = analogRead(SENSOR7);

  Serial.print(sensor0);
  Serial.print("    ");
  Serial.print(sensor1);
  Serial.print("    ");
  Serial.print(sensor2);
  Serial.print("    ");
  Serial.print(sensor3);
  Serial.print("    ");
  Serial.print(sensor4);
  Serial.print("    ");
  Serial.print(sensor5);
  Serial.print("    ");
  Serial.print(sensor6);
  Serial.print("    ");
  Serial.println(sensor7);
  
}
