#include <AStar32U4.h>

AStar32U4ButtonA buttonA;
AStar32U4ButtonB buttonB;
AStar32U4ButtonC buttonC;

void setup()
{
  Serial.begin (9600);
}

void loop()
{
  if (buttonA.isPressed())
  {
    Serial.println("A");
    delay (500);
  }
  if (buttonB.isPressed())
  {
    Serial.println("B");
    delay (500);
  }
  if (buttonC.isPressed())
  {
    Serial.println("C");
    delay (500);
  }
}
