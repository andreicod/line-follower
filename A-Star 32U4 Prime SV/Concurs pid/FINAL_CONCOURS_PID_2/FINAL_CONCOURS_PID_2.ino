#include "IRremote.h"

//calibrarea se face cu ajutorul telecomenzii
//varianta in care diferenta dintre sensori se face in functie de minimul dintre maxime, scazandu-se dif

int nrsensori = 8, rightmotorspeed, leftmotorspeed;
unsigned long error, lasterror, suma;
float difmotoare;

#define X 100 //P% (la suta)
#define Y 10   //I% (la suta)
#define Z 50   //D% (la suta)

#define BASE_SPEED 70

#define NUM_SENSORI 8

#define SENSOR0 A11
#define SENSOR1 A6
#define SENSOR2 A0
#define SENSOR3 A1
#define SENSOR4 A2
#define SENSOR5 A3
#define SENSOR6 A4
#define SENSOR7 A5

#define LED 13

#define RECV_PIN 0

#define digital1 7
#define analog1 9
#define digital2 8
#define analog2 10

long int sensorerror[NUM_SENSORI];
int maxim[NUM_SENSORI];
int minim[NUM_SENSORI];
long int sensor[NUM_SENSORI];
int dif[NUM_SENSORI];
int minmax = 1000;

bool af = 0;

IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  irrecv.enableIRIn();
  
  pinMode(LED, OUTPUT);
  
  pinMode(SENSOR0, INPUT);
  pinMode(SENSOR1, INPUT);
  pinMode(SENSOR2, INPUT);
  pinMode(SENSOR3, INPUT);
  pinMode(SENSOR4, INPUT);
  pinMode(SENSOR5, INPUT);
  pinMode(SENSOR6, INPUT);
  pinMode(SENSOR7, INPUT);

  //Serial.begin(9600);
}


int x, y;
//functie motoare
void motoare(int x, int y)
{
  if (x >= 0)
  {
    digitalWrite (digital1, 0);
    analogWrite (analog1, x);
  }

  if (x < 0)
  {
    digitalWrite (digital1, 1);
    analogWrite (analog1, -x);
  }

  if (y >= 0)
  {
    digitalWrite (digital2, 0);
    analogWrite (analog2, y);
  }

  if (y < 0)
  {
    digitalWrite (digital2, 1);
    analogWrite (analog2, -y);
  }
}

void loop()
{
  if (irrecv.decode(&results)) 
  {
    if (results.value == 1886388479)
    {
      //CALIBRAREA SENSORILOR INAINTE DE CURSA
      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
      //aprindem ledul
      digitalWrite (LED, HIGH);
      
      unsigned long int time = millis();
      unsigned long int starttime = millis();
    
      //minimul incepe de la o valoare cat mai mare (cea mai mare posibila pe care o poate sensorul) ca sa avem din ce scadea
      for (int i = 0; i < NUM_SENSORI; ++ i)
        minim[i] = 1000;
      
      //stabilesc minimurile si maximurile pentru fiecare sensor in parte
      while (time - starttime <= 5000)
      {
        sensor[0] = analogRead(SENSOR0);
        sensor[1] = analogRead(SENSOR1);
        sensor[2] = analogRead(SENSOR2);
        sensor[3] = analogRead(SENSOR3);
        sensor[4] = analogRead(SENSOR4);
        sensor[5] = analogRead(SENSOR5);
        sensor[6] = analogRead(SENSOR6);
        sensor[7] = analogRead(SENSOR7);
    
    
        for (int i = 0; i < NUM_SENSORI; ++ i)
        {
          if (sensor[i] > maxim[i])
            maxim[i] = sensor[i];
          if (sensor[i] < minim[i])
            minim[i] = sensor[i];
        }
        
        time = millis();
      }
    
      //imi iau masuri de siguranta ca, cand sensorul e pe alb valoare lui sa fie 0
      for (int i = 0; i < NUM_SENSORI; ++ i)
        minim[i] = minim[i] + 50;
    
    
      //imi aleg cel mai mic dintre valorile maxime ale sensorilor
      for (int i = 0; i < NUM_SENSORI; ++ i)
        if (maxim[i] < minmax)
          minmax = maxim[i];
    
      //calculam sensorii in functie de minmax; diferenta dintre ei reprezinta dif[i], astfel mai incolo in program  cand din valoare sensorului scadem dif[i] valorile sunt cat mai apropriate
      for (int i = 0; i < NUM_SENSORI; ++ i)
        dif[i] = maxim[i] - minmax;
        
      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }
    
    if (results.value == 1886398679)
      af = !af;
    
    if (af == 1)
      delay (1000);
      
      while  (af == 1)
      {
      
        
      sensor[0] = analogRead(SENSOR0);
      sensor[1] = analogRead(SENSOR1);
      sensor[2] = analogRead(SENSOR2);
      sensor[3] = analogRead(SENSOR3);
      sensor[4] = analogRead(SENSOR4);
      sensor[5] = analogRead(SENSOR5);
      sensor[6] = analogRead(SENSOR6);
      sensor[7] = analogRead(SENSOR7);
  
      //facem calculul final pentru valoarea sensorilor
      for (int i = 0; i < NUM_SENSORI; ++ i)
      {
        sensor[i] = sensor[i] - minim[i] - dif[i];

        sensor[i] = sensor[i] / 10;
        
        if (sensor[i] < 0)
          sensor[i] = 0;
      }

      //AICI SE INTAMPLA MAGIA
      //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      error = 0;
      suma = 0;
      difmotoare = 0;
        
      for (int i = 0; i < NUM_SENSORI; ++ i)
          sensorerror[i] = sensor[i] * ((i + 1) * 100);
      
      for (int i = 0; i < NUM_SENSORI; ++ i)
          suma = suma + sensor[i];
      
      for (int i = 0; i < NUM_SENSORI; ++ i)
        error = error + sensorerror[i];


      if (suma > 0)
      {
        error = error / suma;
        if (error == 450)
          motoare(BASE_SPEED, BASE_SPEED);
        else
          difmotoare = ((X *((450.0 - error) / ((NUM_SENSORI - 1) * 50) * BASE_SPEED)) + (Y * (((450.0 - error) + (450.0 - lasterror)) / ((NUM_SENSORI - 1) * 100) * BASE_SPEED)) + (Z * (((450.0 - error) - (450.0 - lasterror)) / ((NUM_SENSORI - 1) * 50) * BASE_SPEED))) / 100;
      }
      else
      {
        if (lasterror > 450)
          while (error == 0)
          {
            motoare(BASE_SPEED, -1 * BASE_SPEED);
            
            sensor[0] = analogRead(SENSOR0);
            sensor[1] = analogRead(SENSOR1);
            sensor[2] = analogRead(SENSOR2);
            sensor[3] = analogRead(SENSOR3);
            sensor[4] = analogRead(SENSOR4);
            sensor[5] = analogRead(SENSOR5);
            sensor[6] = analogRead(SENSOR6);
            sensor[7] = analogRead(SENSOR7);
  
            //facem calculul final pentru valoarea sensorilor
            for (int i = 0; i < NUM_SENSORI; ++ i)
            {
              sensor[i] = sensor[i] - minim[i] - dif[i];

              sensor[i] = sensor[i] / 10;
        
              if (sensor[i] < 0)
                sensor[i] = 0;
            }
            
            for (int i = 0; i < NUM_SENSORI; ++ i)
              sensorerror[i] = sensor[i] * ((i + 1) * 100);
            for (int i = 0; i < NUM_SENSORI; ++ i)
              error = error + sensorerror[i];

            if (digitalRead (RECV_PIN) == 0)
              af = !af;
          }
        if (lasterror < 450)
          while (error == 0)
          {
            motoare(-1 * BASE_SPEED, BASE_SPEED);
            
            sensor[0] = analogRead(SENSOR0);
            sensor[1] = analogRead(SENSOR1);
            sensor[2] = analogRead(SENSOR2);
            sensor[3] = analogRead(SENSOR3);
            sensor[4] = analogRead(SENSOR4);
            sensor[5] = analogRead(SENSOR5);
            sensor[6] = analogRead(SENSOR6);
            sensor[7] = analogRead(SENSOR7);
  
            //facem calculul final pentru valoarea sensorilor
            for (int i = 0; i < NUM_SENSORI; ++ i)
            {
              sensor[i] = sensor[i] - minim[i] - dif[i];

              sensor[i] = sensor[i] / 10;
        
              if (sensor[i] < 0)
                sensor[i] = 0;
            }
            
            for (int i = 0; i < NUM_SENSORI; ++ i)
              sensorerror[i] = sensor[i] * ((i + 1) * 100);
            for (int i = 0; i < NUM_SENSORI; ++ i)
              error = error + sensorerror[i];
              
            if (digitalRead (RECV_PIN) == 0)
              af = !af;
           }
         if (lasterror == 450)
          motoare(BASE_SPEED, BASE_SPEED);
      }

      //Serial.println (suma);
      
      rightmotorspeed = BASE_SPEED + (int)difmotoare;
      leftmotorspeed = BASE_SPEED - (int)difmotoare;

      if (rightmotorspeed > 255)
        rightmotorspeed = 255;
      if (leftmotorspeed > 255)
        leftmotorspeed = 255;
      if (rightmotorspeed < -255)
        rightmotorspeed = -255;
      if (leftmotorspeed < -255)
        leftmotorspeed = -255;

      motoare (leftmotorspeed, rightmotorspeed);
      
      lasterror = error;
      
      //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
      if (digitalRead (RECV_PIN) == 0)
        af = !af;
    }
    

    if (af == 0)
      motoare (0, 0);
    
    digitalWrite (LED, LOW);
    delay (200);
    irrecv.resume();
  }
  
}

