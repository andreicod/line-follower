//varianta in care diferenta dintre sensori se face in functie de minimul dintre maxime, scazandu-se dif

#define NUM_SENSORI 8

#define SENSOR0 A11
#define SENSOR1 A6
#define SENSOR2 A0
#define SENSOR3 A1
#define SENSOR4 A2
#define SENSOR5 A3
#define SENSOR6 A4
#define SENSOR7 A5

int maxim[NUM_SENSORI];
int minim[NUM_SENSORI];
int sensor[NUM_SENSORI];
int dif[NUM_SENSORI];
int minmax = 1000;


void setup() {
  pinMode(SENSOR0, INPUT);
  pinMode(SENSOR1, INPUT);
  pinMode(SENSOR2, INPUT);
  pinMode(SENSOR3, INPUT);
  pinMode(SENSOR4, INPUT);
  pinMode(SENSOR5, INPUT);
  pinMode(SENSOR6, INPUT);
  pinMode(SENSOR7, INPUT);
  
  Serial.begin(9600);

  //CALIBRAREA SENSORILOR INAINTE DE CURSA
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  unsigned long int time = millis();
  unsigned long int starttime = millis();

  //minimul incepe de la o valoare cat mai mare (cea mai mare posibila pe care o poate sensorul) ca sa avem din ce scadea
  for (int i = 0; i < NUM_SENSORI; ++ i)
    minim[i] = 1000;
  
  //stabilesc minimurile si maximurile pentru fiecare sensor in parte
  while (time - starttime <= 5000)
  {
    sensor[0] = analogRead(SENSOR0);
    sensor[1] = analogRead(SENSOR1);
    sensor[2] = analogRead(SENSOR2);
    sensor[3] = analogRead(SENSOR3);
    sensor[4] = analogRead(SENSOR4);
    sensor[5] = analogRead(SENSOR5);
    sensor[6] = analogRead(SENSOR6);
    sensor[7] = analogRead(SENSOR7);


    for (int i = 0; i < NUM_SENSORI; ++ i)
    {
      if (sensor[i] > maxim[i])
        maxim[i] = sensor[i];
      if (sensor[i] < minim[i])
        minim[i] = sensor[i];
    }
    
    time = millis();
  }

  //imi iau masuri de siguranta ca, cand sensorul e pe alb valoare lui sa fie 0
  for (int i = 0; i < NUM_SENSORI; ++ i)
    minim[i] = minim[i] + 10;


  //imi aleg cel mai mic dintre valorile maxime ale sensorilor
  for (int i = 0; i < NUM_SENSORI; ++ i)
    if (maxim[i] < minmax)
      minmax = maxim[i];

  //calculam sensorii in functie de minmax; diferenta dintre ei reprezinta dif[i], astfel mai incolo in program  cand din valoare sensorului scadem dif[i] valorile sunt cat mai apropriate
  for (int i = 0; i < NUM_SENSORI; ++ i)
    dif[i] = maxim[i] - minmax;
    
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

void loop()
{
  //facem un test de citire a sensorilor ca in cursa
  sensor[0] = analogRead(SENSOR0);
  sensor[1] = analogRead(SENSOR1);
  sensor[2] = analogRead(SENSOR2);
  sensor[3] = analogRead(SENSOR3);
  sensor[4] = analogRead(SENSOR4);
  sensor[5] = analogRead(SENSOR5);
  sensor[6] = analogRead(SENSOR6);
  sensor[7] = analogRead(SENSOR7);
  


  //facem calculele pentru sensori
  for (int i = 0; i < NUM_SENSORI; ++ i)
  {
    sensor[i] = sensor[i] - minim[i] - dif[i];
    
    if (sensor[i] < 0)
      sensor[i] = 0;
  }

  for (int i = 0; i < NUM_SENSORI; ++ i)
  {
    Serial.print (sensor[i]);
    Serial.print ("   ");
  }
  Serial.println();
  
  delay(1);
}


