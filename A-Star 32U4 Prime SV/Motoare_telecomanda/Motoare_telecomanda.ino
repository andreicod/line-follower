#include "IRremote.h"

bool af = 0;
int RECV_PIN = 0;
int x, y;

#define digital1 7
#define analog1 9
#define digital2 8
#define analog2 10

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  irrecv.enableIRIn();
  pinMode (7, OUTPUT);
  pinMode (8, OUTPUT);
  pinMode (9, OUTPUT);
  pinMode (10, OUTPUT);
}

void motoare(int x, int y)
{
  if (x >= 0)
  {
    digitalWrite (digital1, 0);
    analogWrite (analog1, x);
  }

  if (x < 0)
  {
    digitalWrite (digital1, 1);
    analogWrite (analog1, -x);
  }

  if (y >= 0)
  {
    digitalWrite (digital2, 0);
    analogWrite (analog2, y);
  }

  if (y < 0)
  {
    digitalWrite (digital2, 1);
    analogWrite (analog2, -y);
  }
}

void loop() {
  if (irrecv.decode(&results)) 
  {
    if (results.value = 1886398679)
      af = !af;

    if (af == 1)
      motoare (50, 50);

    if (af == 0)
      motoare (0, 0);
     
    delay (200);
    irrecv.resume();
  }
}
