#define digital1 7
#define analog1 9
#define digital2 8
#define analog2 10

void motoare(int x, int y)
{
  if (x >= 0)
  {
    digitalWrite (digital1, 0);
    analogWrite (analog1, x);
  }

  if (x < 0)
  {
    digitalWrite (digital1, 1);
    analogWrite (analog1, -x);
  }

  if (y >= 0)
  {
    digitalWrite (digital2, 0);
    analogWrite (analog2, y);
  }

  if (y < 0)
  {
    digitalWrite (digital2, 1);
    analogWrite (analog2, -y);
  }
}

void setup() {
  pinMode (7, OUTPUT);
  pinMode (8, OUTPUT);
  pinMode (9, OUTPUT);
  pinMode (10, OUTPUT);

  delay(10000);
  
  int i;
  for (i = 0; i <= 255; ++i)
  {
    motoare (i, i);
    delay(10);
  }
  for (i = 255; i >= -255; --i)
  {
    motoare(i, i);
    delay(10);
  }

  for (i = -255; i <= 0; ++ i)
  {
    motoare (i, i);
    delay(10);
  }
  
  motoare (0, 0);
}
void loop() {
  // put your main code here, to run repeatedly:

}
