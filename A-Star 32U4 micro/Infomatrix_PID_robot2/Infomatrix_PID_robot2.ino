#include "IRremote.h"

//calibrarea se face cu ajutorul telecomenzii
//varianta in care diferenta dintre sensori se face in functie de minimul dintre maxime, scazandu-se dif

int dir;
int nrsensori = 8, rightmotorspeed, leftmotorspeed;
unsigned long error, lasterror, suma;
double difmotoare;
//traseu mic
//88 11 35 175
//88  8 35 160

//traseu mare
//88 6 35 150
//50 1 48 150

//traseu nou
//5 15 150 150
//5 16 350 150
//5 16 370 150
//2 25 400 150

//PID perfect
//40 10 165 180 traseu cu curbe line

double P = 40.0; //P% (la suta) 
double I = 60.0; //I% (la suta)
double D = 1500.0; //D% (la suta)

double BASE_SPEED = 160;

#define LEFT_PROPORTION 1.00
#define RIGHT_PROPORTION 1.01

#define NUM_SENSORI 8

//pini senzor linefollower
#define SENSOR0 A6
#define SENSOR1 A7
#define SENSOR2 A8
#define SENSOR3 A9
#define SENSOR4 A11
#define SENSOR5 A10
#define SENSOR6 A1
#define SENSOR7 A0

#define LED 13

#define RECV_PIN 0

//pini control motoare
#define motorpwm1 11
#define motordigital1 7
#define motorpwm2 5
#define motordigital2 3

long int sensorerror[NUM_SENSORI];
int maxim[NUM_SENSORI];
int minim[NUM_SENSORI];
long int sensor[NUM_SENSORI];
int dif[NUM_SENSORI];
int minmax = 1000;

bool af = 0;
int i = 0;

IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {  
  irrecv.enableIRIn();
  
  pinMode(LED, OUTPUT);
  
  pinMode(SENSOR0, INPUT);
  pinMode(SENSOR1, INPUT);
  pinMode(SENSOR2, INPUT);
  pinMode(SENSOR3, INPUT);
  pinMode(SENSOR4, INPUT);
  pinMode(SENSOR5, INPUT);
  pinMode(SENSOR6, INPUT);
  pinMode(SENSOR7, INPUT);

  pinMode(motorpwm1, OUTPUT);
  pinMode(motordigital1, OUTPUT);
  pinMode(motorpwm2, OUTPUT);
  pinMode(motordigital2, OUTPUT);

  //Serial.begin(9600);
}


int x, y;
void motoare (int x, int y){
  x = x * LEFT_PROPORTION;
  y = y * RIGHT_PROPORTION;
  
  if (y < -255)
    y = -255;
  if (x < -255)
    x = -255;
  if (y > 255)
    y = 255;
  if (x > 255)
    x = 255;
  if (x >= 0){
    analogWrite (motorpwm1, x);
    digitalWrite (motordigital1, 0);
    }

  if (x < 0){
    analogWrite (motorpwm1, -x);
    digitalWrite (motordigital1, 1);
  }

  if (y >= 0){
    analogWrite (motorpwm2, y);
    digitalWrite (motordigital2, 1);
  }

  if (y < 0){
    analogWrite (motorpwm2, -y);
    digitalWrite (motordigital2, 0);
  }
}

void loop()
{
  if (irrecv.decode(&results)) 
  {

    //Serial.println(results.value);

    if (results.value == 1168){
      digitalWrite (LED, HIGH);
      BASE_SPEED ++;
      //Serial.println(BASE_SPEED);
      digitalWrite (LED, LOW);
    }

    if (results.value == 3216){
      digitalWrite (LED, HIGH);
      BASE_SPEED --;
      //Serial.println(BASE_SPEED);
      digitalWrite (LED, LOW);
    }

    if (results.value == 3088){
      digitalWrite (LED, HIGH);
      P ++;
      //Serial.println(P);
      digitalWrite (LED, LOW);
    }

    if (results.value == 1552){
      digitalWrite (LED, HIGH);
      P --;
      //Serial.println(P);
      digitalWrite (LED, LOW);
    }

    if (results.value == 528){
      digitalWrite (LED, HIGH);
      I ++;
      //Serial.println(I);
      digitalWrite (LED, LOW);
    }

    if (results.value == 3600){
      digitalWrite (LED, HIGH);
      I --;
      //Serial.println(I);
      digitalWrite (LED, LOW);
    }

    if (results.value == 2576){
      digitalWrite (LED, HIGH);
      D ++;
      //Serial.println(D);
      digitalWrite (LED, LOW);
    }

    if (results.value == 272){
      digitalWrite (LED, HIGH);
      D --;
      //Serial.println(D);
      digitalWrite (LED, LOW);
    }

    //1886388479
    if (results.value == 16)
    {
      //CALIBRAREA SENSORILOR INAINTE DE CURSA
      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
      //aprindem ledul
      digitalWrite (LED, HIGH);
      
      unsigned long int time = millis();
      unsigned long int starttime = millis();
    
      //minimul incepe de la o valoare cat mai mare (cea mai mare posibila pe care o poate sensorul) ca sa avem din ce scadea
      for (i = 0; i < NUM_SENSORI; ++ i)
        minim[i] = 1000;

      for (i = 0; i < NUM_SENSORI; ++ i)
        maxim[i] = 0;
            
      //stabilesc minimurile si maximurile pentru fiecare sensor in parte
      while (time - starttime <= 5000)
      {
        sensor[0] = analogRead(SENSOR0);
        sensor[1] = analogRead(SENSOR1);
        sensor[2] = analogRead(SENSOR2);
        sensor[3] = analogRead(SENSOR3);
        sensor[4] = analogRead(SENSOR4);
        sensor[5] = analogRead(SENSOR5);
        sensor[6] = analogRead(SENSOR6);
        sensor[7] = analogRead(SENSOR7);
    
    
        for (i = 0; i < NUM_SENSORI; ++ i)
        {
          if (sensor[i] > maxim[i])
            maxim[i] = sensor[i];
          if (sensor[i] < minim[i])
            minim[i] = sensor[i];
        }
        
        time = millis();
      }
    
      //imi iau masuri de siguranta ca, cand sensorul e pe alb valoare lui sa fie 0
      for (i = 0; i < NUM_SENSORI; ++ i)
        minim[i] = minim[i] + 30;
    
    
      //imi aleg cel mai mic dintre valorile maxime ale sensorilor
      for (i = 0; i < NUM_SENSORI; ++ i)
        if (maxim[i] < minmax)
          minmax = maxim[i];
    
      //calculam sensorii in functie de minmax; diferenta dintre ei reprezinta dif[i], astfel mai incolo in program  cand din valoare sensorului scadem dif[i] valorile sunt cat mai apropriate unele de celelalte
      //a nu se uita ca imi iau si aici o marja de eroare
      for (i = 0; i < NUM_SENSORI; ++ i)
        dif[i] = maxim[i] - minmax + 30;
        
      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }

    //1886398679
    if (results.value == 2704)
      af = !af;
    
    if (af == 1)
    {
      delay (1000);
      error = 0;
    }
      
      while  (af == 1)
      {

      //AICI SE INTAMPLA MAGIA
      //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      


      while (error >= 100 && af == 1)
      {
        
        sensor[0] = analogRead(SENSOR0);
        sensor[1] = analogRead(SENSOR1);
        sensor[2] = analogRead(SENSOR2);
        sensor[3] = analogRead(SENSOR3);
        sensor[4] = analogRead(SENSOR4);
        sensor[5] = analogRead(SENSOR5);
        sensor[6] = analogRead(SENSOR6);
        sensor[7] = analogRead(SENSOR7);
    
        //facem calculul final pentru valoarea sensorilor
        for (i = 0; i < NUM_SENSORI; ++ i)
        {
          sensor[i] = sensor[i] - minim[i] - dif[i];

          if (sensor[i] < 0)
             sensor[i] = 0;
          
          sensor[i] = sensor[i] / 10;
        }
        
        error = 0;
        suma = 0;
          
        for (i = 0; i < NUM_SENSORI; ++ i)
            sensorerror[i] = sensor[i] * ((i + 1) * 100);
        
        for (i = 0; i < NUM_SENSORI; ++ i)
            suma = suma + sensor[i];
        
        for (i = 0; i < NUM_SENSORI; ++ i)
          error = error + sensorerror[i];
        
        if (suma > 0)
        {
          error = error / suma;
          difmotoare = ((P *((450.0 - error) / ((NUM_SENSORI - 1) * 50) * BASE_SPEED)) + (I * (((450.0 - error) + (450.0 - lasterror)) / ((NUM_SENSORI - 1) * 50) * BASE_SPEED)) + (D * (((450.0 - error) - (450.0 - lasterror)) / ((NUM_SENSORI - 1) * 50) * BASE_SPEED))) / 100;
        
        rightmotorspeed = BASE_SPEED + (int)difmotoare;
        leftmotorspeed = BASE_SPEED - (int)difmotoare;
  
        motoare (leftmotorspeed, rightmotorspeed);
        //Serial.println("PID");

        lasterror = error;
        }
        
        if (digitalRead (RECV_PIN) == 0)
              af = !af;
      }

    
      if (lasterror > 450 && af == 1)
        while (error == 0 && af == 1)
        {
          motoare(BASE_SPEED * 7 / 6, -BASE_SPEED / 6 * 4);
          //Serial.println("DREAPTA");

          dir = 3;
            
          sensor[0] = analogRead(SENSOR0);
          sensor[1] = analogRead(SENSOR1);
          sensor[2] = analogRead(SENSOR2);
          sensor[3] = analogRead(SENSOR3);
          sensor[4] = analogRead(SENSOR4);
          sensor[5] = analogRead(SENSOR5);
          sensor[6] = analogRead(SENSOR6);
          sensor[7] = analogRead(SENSOR7);
  
          //facem calculul final pentru valoarea sensorilor
          for (i = 0; i < NUM_SENSORI; ++ i)
          {
            sensor[i] = sensor[i] - minim[i] - dif[i];

            sensor[i] = sensor[i] / 10;
        
            if (sensor[i] < 0)
              sensor[i] = 0;
          }
            
          for (i = 0; i < NUM_SENSORI; ++ i)
            sensorerror[i] = sensor[i] * ((i + 1) * 100);
          for (i = 0; i < NUM_SENSORI; ++ i)
            error = error + sensorerror[i];
            
          if (digitalRead (RECV_PIN) == 0)
            af = !af;
        }
        
        if (lasterror <= 450 && af == 1)
          while (error == 0 && af == 1)             
          {
            motoare(-BASE_SPEED / 6 * 4, BASE_SPEED * 7 / 6);
            Serial.println("STANGA");
            
            dir = 1;
            
            sensor[0] = analogRead(SENSOR0);
            sensor[1] = analogRead(SENSOR1);
            sensor[2] = analogRead(SENSOR2);
            sensor[3] = analogRead(SENSOR3);
            sensor[4] = analogRead(SENSOR4);
            sensor[5] = analogRead(SENSOR5);
            sensor[6] = analogRead(SENSOR6);
            sensor[7] = analogRead(SENSOR7);
  
            //facem calculul final pentru valoarea sensorilor
            for (i = 0; i < NUM_SENSORI; ++ i)
            {
              sensor[i] = sensor[i] - minim[i] - dif[i];

              sensor[i] = sensor[i] / 10;
        
              if (sensor[i] < 0)
                sensor[i] = 0;
            }
            
            for (i = 0; i < NUM_SENSORI; ++ i)
              sensorerror[i] = sensor[i] * ((i + 1) * 100);
            for (i = 0; i < NUM_SENSORI; ++ i)
              error = error + sensorerror[i];
              
            if (digitalRead (RECV_PIN) == 0)
              af = !af;
           }
      
      suma = 0;
      for (i = 0; i < NUM_SENSORI; ++ i)
        suma = suma + sensor[i];
      if (suma > 0)
      {
        error = error / suma;
        lasterror = error;
      }

      if (dir == 1){
        motoare(BASE_SPEED * 8 / 6, -BASE_SPEED / 2);
        //Serial.println("REVENIRE DIN STANGA");
      }
      if (dir == 3){
        motoare(-BASE_SPEED / 2, BASE_SPEED * 8 / 6);
        //Serial.println("REVENIRE DIN DREAPTA");
      }

      delay(20);
      
      //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
      if (digitalRead (RECV_PIN) == 0)
        af = !af;
    }
    
    
    
    if (af == 0)
      motoare (0, 0);
    
    digitalWrite (LED, LOW);
    delay (200);
    irrecv.resume();
  }
  
}

