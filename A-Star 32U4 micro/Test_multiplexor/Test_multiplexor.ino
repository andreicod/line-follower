#define SIG A0
#define S0 0
#define S1 1
#define S2 2
#define S3 3

int signal(int port) {
  for (int i = 0; i < 4; ++ i) {
    digitalWrite(i, port & 1);
    port >>= 1;
  }
  return analogRead(SIG);
}


void setup() {
  pinMode(SIG, INPUT);
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  for (int i = 8; i < 16; ++ i) {
    Serial.print(signal(i));
    Serial.print("        ");
  }
  Serial.println();
}
