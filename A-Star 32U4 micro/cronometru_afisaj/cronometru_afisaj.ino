#include <Wire.h>

#define DISPLAY_ADDRESS1 0x71 //This is the default address of the OpenSegment with both solder jumpers open
const int S_PIN = 12;

int cycles = 0;

void setup() 
{
  pinMode(S_PIN, INPUT);
  
  Wire.begin(); //Join the bus as master

  //By default .begin() will set I2C SCL to Standard Speed mode of 100kHz
  //Wire.setClock(400000); //Optional - set I2C SCL to High Speed Mode of 400kHz

  //Send the reset command to the display - this forces the cursor to return to the beginning of the display
  Wire.beginTransmission(DISPLAY_ADDRESS1);
  Wire.write('v');
  Wire.endTransmission();
}

bool running = false;


void loop() 
{
  while (!running) {
    if (!digitalRead(S_PIN))
      running = !running;
  }

  cycles = 0;
  while (running) {
    cycles++; //Counting cycles! Yay!
    i2cSendValue(cycles / 10); //Send the four characters to the display

    if (!digitalRead(S_PIN) && cycles >= 1000)
      running = !running;
    
    delay(1); //If we remove the slow debug statements, we need a very small delay to prevent flickering
  }

  delay (2000);
}

//Given a number, i2cSendValue chops up an integer into four values and sends them out over I2C
void i2cSendValue(int tempCycles)
{
  Wire.beginTransmission(DISPLAY_ADDRESS1); // transmit to device #1
  Wire.write(tempCycles / 1000); //Send the left most digit
  tempCycles %= 1000; //Now remove the left most digit from the number we want to display
  Wire.write(tempCycles / 100);
  tempCycles %= 100;
  Wire.write(tempCycles / 10);
  tempCycles %= 10;
  Wire.write(tempCycles); //Send the right most digit
  Wire.write(0x77);  // Decimal control command
  Wire.write(0b00000010);  // Turns on colon, apostrophoe, and far-right decimal
  Wire.endTransmission(); //Stop I2C transmission
}
