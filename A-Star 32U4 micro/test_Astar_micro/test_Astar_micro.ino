#include "IRremote.h"

//pini control motoare
#define motorpwm1 11
#define motordigital1 7
#define motorpwm2 5
#define motordigital2 3

//pini senzor linefollower
#define SENSOR0 A6
#define SENSOR1 A7
#define SENSOR2 A8
#define SENSOR3 A9
#define SENSOR4 A11
#define SENSOR5 A10
#define SENSOR6 A1
#define SENSOR7 A0

//pin senzor telecomanda
#define telecomanda 0

int RECV_PIN = 0;
IRrecv irrecv(RECV_PIN);

decode_results results;

void motoare (double x, double y){
  if (x >= 0){
    analogWrite (motorpwm1, x);
    digitalWrite (motordigital1, 0);
    }

  if (x < 0){
    analogWrite (motorpwm1, -x);
    digitalWrite (motordigital1, 1);
  }

  if (y >= 0){
    analogWrite (motorpwm2, y);
    digitalWrite (motordigital2, 1);
  }

  if (y < 0){
    analogWrite (motorpwm2, -y);
    digitalWrite (motordigital2, 0);
  }
}

void setup() {
  irrecv.enableIRIn();
  Serial.begin(9600);
  
  pinMode (motorpwm1, OUTPUT);
  pinMode (motordigital1, OUTPUT);
  pinMode (motorpwm2, OUTPUT);
  pinMode (motordigital2, OUTPUT);
  
  pinMode (SENSOR0, INPUT);
  pinMode (SENSOR1, INPUT);
  pinMode (SENSOR2, INPUT);
  pinMode (SENSOR3, INPUT);
  pinMode (SENSOR4, INPUT);
  pinMode (SENSOR5, INPUT);
  pinMode (SENSOR6, INPUT);
  pinMode (SENSOR7, INPUT);

  pinMode (13, OUTPUT);

  pinMode (telecomanda, INPUT);

  delay (2000);

  digitalWrite (13, HIGH);

  //test motoare
  /*
    int i;
  for (i = 0; i <= 200; ++i)
  {
    motoare (i, i);
    delay(10);
  }
  for (i = 200; i >= -200; --i)
  {
    motoare(i, i);
    delay(10);
  }

  for (i = -200; i <= 0; ++ i)
  {
    motoare (i, i);
    delay(10);
  }
  delay (5000);

  digitalWrite (13, LOW);
  motoare (0, 0);
  */
  
}
void loop() {
  //test senzori culoare

  
  int sensor[8];
  sensor[0] = analogRead(SENSOR0);
  sensor[1] = analogRead(SENSOR1);
  sensor[2] = analogRead(SENSOR2);
  sensor[3] = analogRead(SENSOR3);
  sensor[4] = analogRead(SENSOR4);
  sensor[5] = analogRead(SENSOR5);
  sensor[6] = analogRead(SENSOR6);
  sensor[7] = analogRead(SENSOR7);

  for (int i = 0; i < 8; ++ i){
    Serial.print(sensor[i]);
    Serial.print("    ");
  }

  Serial.println();
  

  //senzor telecomanda

  /*
  if (irrecv.decode(&results)) 
  {
    Serial.println(results.value);
    delay (200);
    irrecv.resume();
  }
  */
  
}
