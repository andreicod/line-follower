#include <Servo.h>

#define MAX_SIGNAL 2000
#define MIN_SIGNAL 700
#define MOTOR_PIN 9

Servo motor;

void setup() {
  Serial.begin(9600);
  Serial.println("Program begin...");
  Serial.println("This program will calibrate the ESC.");

  motor.attach(MOTOR_PIN);

  Serial.println("Now writing maximum output.");
  Serial.println("Turn on power source, then wait 2 seconds and press any key.");
  motor.writeMicroseconds(MAX_SIGNAL);

  // Wait for input
  while (!Serial.available());
  Serial.read();

  // Send min output
  Serial.println("Sending minimum output");
  motor.writeMicroseconds(MIN_SIGNAL);

  delay (2000);
}

void loop() {
  int val;
  val = analogRead(A0);
  val = map(val, 0, 1023, 700, 2000);
  motor.writeMicroseconds(val);
  Serial.println(val);
}


//void setup(){
//  pinMode(A0, INPUT);
//  Serial.begin(9600);
//}
//
//void loop(){
//  int val = analogRead(A0);
//  Serial.println(val);
//}
