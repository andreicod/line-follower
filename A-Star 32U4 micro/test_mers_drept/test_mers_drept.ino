//pini control motoare
#define motorpwm1 11
#define motordigital1 7
#define motorpwm2 5
#define motordigital2 3

void motoare (double x, double y){
  if (x >= 0){
    analogWrite (motorpwm1, x);
    digitalWrite (motordigital1, 0);
    }

  if (x < 0){
    analogWrite (motorpwm1, -x);
    digitalWrite (motordigital1, 1);
  }

  if (y >= 0){
    analogWrite (motorpwm2, y);
    digitalWrite (motordigital2, 1);
  }

  if (y < 0){
    analogWrite (motorpwm2, -y);
    digitalWrite (motordigital2, 0);
  }
}

void setup() {
  // put your setup code here, to run once:
  pinMode (motorpwm1, OUTPUT);
  pinMode (motordigital1, OUTPUT);
  pinMode (motorpwm2, OUTPUT);
  pinMode (motordigital2, OUTPUT);

  delay(1000);
  motoare (100, 100);
  delay(1000);
  motoare (0, 0);
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
