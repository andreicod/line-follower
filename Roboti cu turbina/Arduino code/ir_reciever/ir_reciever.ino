const int IR_PIN = 0;

class IrReciever {
  int pin = 0;
  bool state;
public:
  IrReciever(int _pin = 0) {
    pin = _pin;
  }

  void setPin(int _pin) {
    (*this) = IrReciever(_pin);    
  }
  void init() {
    pinMode(pin, INPUT);
  }

  void update() {
    state = digitalRead(pin);
  }

  bool getState() {
    update();
    return state;
  }
  
};

IrReciever irReciever(IR_PIN);

void setup() {
  Serial.begin(9600);
  irReciever.init();
}

void loop() {
  Serial.println(irReciever.getState());
}
