const int CNT_LINE_SENSORS = 8;
const int LINE_SENSORS_PINS[CNT_LINE_SENSORS] = {A6, A7, A8, A9, A11, A10, A1, A0};

class LineSensor {
	int pin, rawVal;
	bool state;
	int threshold = 200;
public:
	LineSensor(int _pin = 0) {
		pin = _pin;
	}

	void setPin(int _pin) {
		(*this) = LineSensor(_pin);
	}

  void init() {
    pinMode(pin, INPUT);
  }

	void update() {
		rawVal = analogRead(pin);
		if (rawVal > threshold) state = 1;
		else state = 0;
	}

	int getRaw() {
		update();
		return rawVal;
	}

	int getRawWithoutUpdate() {
		return rawVal;
	}

	bool getState() {
		update();
		return state;
	}

	void setThreshold(int value) {
		threshold = value;
	}
};

class LineSensors {
	int n = CNT_LINE_SENSORS;
	int *lineSensorsPins = LINE_SENSORS_PINS;
	LineSensor ls[CNT_LINE_SENSORS];
	int pos = 0;
	int lowerBound[CNT_LINE_SENSORS], upperBound[CNT_LINE_SENSORS], dif[CNT_LINE_SENSORS];

public:
  	LineSensors() {}

	void setSensor(int poz, int _pin) {
		ls[poz].setPin(_pin);
	}

	void setSensors(int v[]) {
		for (int i = 0; i < n; ++ i){ 
		  setSensor(i, v[i]);
		  ls[i].init();
		}
	}

	void init() {
		setSensors(lineSensorsPins);
	}

	void start() {
		updatePosition();
	}

	// De incercat si varianta cu "map" pt calibrare
	void calibrate(unsigned long int time) {
		unsigned long int currentTime = millis();
		unsigned long int startTime = millis();

		for (int i = 0; i < n; ++ i) lowerBound[i] = 1024, upperBound[i] = 0;

		while (currentTime - startTime < time) {
			for (int i = 0; i < n; ++ i) lowerBound[i] = min(lowerBound[i], ls[i].getRaw());
			for (int i = 0; i < n; ++ i) upperBound[i] = max(upperBound[i], ls[i].getRaw());
			currentTime = millis();
		}

		int minMax = 1024;
		for (int i = 0; i < n; ++ i) lowerBound[i] += 30;
		for (int i = 0; i < n; ++ i) minMax = min(minMax, upperBound[i]);
		for (int i = 0; i < n; ++ i) dif[i] = upperBound[i] - minMax + 30;
		for (int i = 0; i < n; ++ i) ls[i].setThreshold(lowerBound[i] + dif[i]);
	}

	int getError() {
		int sum = 0, error = 0;
		for (int i = 0; i < n; ++ i) {
			error += max(ls[i].getRaw() - lowerBound[i] - dif[i], 0) / 10 * ((i + 1) * 100);
			sum += max(ls[i].getRawWithoutUpdate() - lowerBound[i] - dif[i], 0) / 10;
		}

		if (sum == 0) return -1;
		else return error / sum;
	}

	int getFastError() {
		int sum = 0, error = 0;
		for (int i = pos; i <= pos + 1 && i < n; ++ i) {
			error += max(ls[i].getRaw() - lowerBound[i] - dif[i], 0) / 10 * ((i + 1) * 100);
			sum += max(ls[i].getRawWithoutUpdate() - lowerBound[i] - dif[i], 0) / 10;
		}
		if (sum == 0) return -1;
		else {
			int ans = error / sum;
			if (ans == ((pos + 1) + 1) * 100 && pos + 1 < n - 1) ++ pos;
			else if (ans == (pos + 1) * 100 && pos > 0) -- pos;
			return ans;
		}
	}

	void updatePosition() {
		pos = -1;
		for (int i = 0; i < n; ++ i)
			if (ls[i].getState()) {
				pos = i;
				break;
			}
	}

	int getPosition() {
		updatePosition();
		return pos;
	}

	int getPositionWithoutUpdate() {
		return pos;
	}

	bool isOnLine() {
		if (getPosition() != -1)
			return 1;
		return 0;
	}

	void printPosition() {
		Serial.println(getPosition());
	}

	void printSensorsValues() {
		for (int i = 0; i < n; ++ i) {
			Serial.print(ls[i].getState());
			Serial.print("		");
		}
		Serial.println();
	}

	void printSensorsRawValues() {
		for (int i = 0; i < n; ++ i) {
			Serial.print(ls[i].getRaw());
			Serial.print("		");
		}
		Serial.println();
	}
};

class Robot {

public:
	LineSensors line;
	void init() {
		line.init();
	}
};
Robot robot;

void setup() {
	Serial.begin(9600);
  robot.init();
  robot.line.calibrate(5000);
  robot.line.start();
}

void loop() {
	Serial.println(robot.line.getFastError());
}
