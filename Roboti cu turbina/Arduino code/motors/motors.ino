const int CNT_MOTORS = 2;

class Motor {
	int digitalPin, pwmPin;
	int power = 0, dir = 0;
	bool reverse = 0;
public:
	Motor(int _digitalPin = 0, int _pwmPin = 0, bool _reverse = false) {
		digitalPin = _digitalPin;
		pwmPin = _pwmPin;
		reverse = _reverse;
		pinMode(digitalPin, OUTPUT);
		pinMode(pwmPin, OUTPUT);
		digitalWrite(digitalPin, 0);
		analogWrite(pwmPin, 0);
	}

	void setPins(int _digitalPin, int _pwmPin, bool _reverse) {
		(*this) = Motor(_digitalPin, _pwmPin, _reverse);
	}

	void setPower(int val) {
		if (reverse) val = -val;
		if (val > 0) digitalWrite(digitalPin, 1);
		else digitalWrite(digitalPin, 0);
		val = abs(val);
		val = min(val, 255);
		analogWrite(pwmPin, val);
	}

	void stop() {
		digitalWrite(digitalPin, 0);
		analogWrite(pwmPin, 0);
	}
};

class Motors {
	int n = CNT_MOTORS;
	int power[CNT_MOTORS];
	Motor m[CNT_MOTORS];
	int digitalPins[CNT_MOTORS] = {12, 13};
	int pwmPins[CNT_MOTORS] = {3, 11};
	bool reverse[CNT_MOTORS] = {1, 1};
	double differentials[CNT_MOTORS];

public:
	Motors() {
		for (int i = 0; i < n; ++ i) differentials[i] = 1;
	}

	void setMotor(int poz, int _digitalPin, int _pwmPin, bool _reverse) {
		m[poz].setPins(_digitalPin, _pwmPin, _reverse);
	}

	void setMotors(int v1[], int v2[], bool v3[]) {
		for (int i = 0; i < n; ++ i) {
			m[i].setPins(v1[i], v2[i], v3[i]);
		}
	}

	void init() {
		setMotors(digitalPins, pwmPins, reverse);
	}

	void setDifferential(int poz, double ratio) {
		differentials[poz] = ratio;
	}

	void setDifferentials(int ratios[]) {
		for (int i = 0; i < n; ++ i) differentials[i] = ratios[i];
	}

	void setMotorPower(int poz, int val) {
		m[poz].setPower(val);
	}

	void setMotorsPowers(int v[]) {
		for (int i = 0; i < n; ++ i) m[i].setPower(v[i]);
	}
	
	void motoare(int a, int b) {
		m[0].setPower(int(a * differentials[0]));
		m[1].setPower(int(b * differentials[1]));
	}

	void stop() {
		for (int i = 0; i < n; ++ i) m[i].stop();
	}
};

class Robot {
public:
  Motors drive;
	void init() {
		drive.init();
	}
};
Robot robot;

void setup() {
	Serial.begin(9600);
  	robot.init();
}

void loop() {
	for (int i = 0; i < 256; ++ i) {
		robot.drive.motoare(i, i);
		delay(10);
	}
	for (int i = 255; i >= 0; -- i) {
		robot.drive.motoare(i, i);
		delay(10);
	}
	for (int i = 0; i < 256; ++ i) {
		robot.drive.motoare(-i, -i);
		delay(10);
	}
	for (int i = 255; i >= 0; -- i) {
		robot.drive.motoare(-i, -i);
		delay(10);
	}
}
