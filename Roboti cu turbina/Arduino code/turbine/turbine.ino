#include <Servo.h>

const int TURBINE_PIN = 9;

class Turbine {
	int pin;
	int normalPower = 700; //between 0 and 1000
	int MAX_SIGNAL = 2000, MIN_SIGNAL = 700;
	Servo motor;

public:
	Turbine(int _pin = 0) {
    pin = _pin;
	}

  void setPin(int _pin) {
    (*this) = Turbine(_pin);
  }

	void init() {
		motor.attach(pin);
	}

	void calibrate() {
		motor.writeMicroseconds(MAX_SIGNAL);
		unsigned long int startTime = millis();
		unsigned long int currentTime = millis();
		while (currentTime - startTime < 3000) currentTime = millis();
		motor.writeMicroseconds(MIN_SIGNAL);
	}

	//power is between 0 and 1000
	void setPower(int power) {
		power = constrain(power, 0, 1000);
		power = map(power, 0, 1000, MIN_SIGNAL, MAX_SIGNAL);
		motor.writeMicroseconds(power);
	}

	void start() {
		setPower(normalPower);
	}

	void slowStart(unsigned long int time = 2000) {
		unsigned long int startTime = millis();
		unsigned long int currentTime = millis();

		int cntSteps = time / 100;
		int stepPower = normalPower / cntSteps;
		int step;

		while (currentTime - startTime < time) {
			step = (currentTime - startTime) / 100;
			setPower(step * stepPower);
			currentTime = millis();
		}

		setPower(normalPower);
	}

	void stop() {
		setPower(0);
	}

};
Turbine turbine(TURBINE_PIN);

class Robot {
public:
	void init() {
		turbine.init();
		turbine.calibrate();
	}
};

Robot robot;

void setup() {
	robot.init();
}

void loop() {
	turbine.slowStart();
	delay(1000);
	turbine.stop();
}
